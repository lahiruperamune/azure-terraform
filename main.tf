resource "azurerm_resource_group" "example" {
  name     = "terraform-servicebus"
  location = var.location
}

resource "azurerm_servicebus_namespace" "example" {
  name                = "tfex-servicebus-namespace"
  location            = var.location
  resource_group_name = var.resource_group
  sku                 = "Standard"

  tags = {
    source = "terraform"
  }
}
