# Azure Provider
provider "azurerm" {
  version = "=2.0.0"
  features {}
}

# State Backend
terraform {
  backend "azurerm" {
    resource_group_name   = "yourresourcegroup"
    storage_account_name  = "yourstorageaccount"
    container_name        = "terraform-state"
    key                   = "test.terraform.tfstate"
  }
}
